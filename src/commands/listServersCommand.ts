// Copyright (C) 2020 Mathias Zech
// This file is part of ArkRconBot <https://gitlab.com/mze9412/arkrconbot>.
//
// ArkRconBot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArkRconBot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArkRconBot. If not, see <http://www.gnu.org/licenses/>.

import { Message } from "discord.js";
import { CommandoClient, CommandoMessage } from "discord.js-commando";
import { DatabaseService } from "../services/database.service";
import { RconBotCommand } from "./rconBotCommand";

export class ListServersCommand extends RconBotCommand {
	constructor(client: CommandoClient) {
		super(client, {
			name: 'listservers',
			aliases: ['list-servers'],
			group: 'rcon',
			memberName: 'listservers',
			description: 'Lists all registered Ark servers for your Discord.',
            guildOnly: true
		});
    }
    
	public async run(msg: CommandoMessage): Promise<Message | Message[]> {
        if (! await this.hasAccess(msg)) {
            return msg.reply('Sorry, you are not allowed to use this command.');
        }

        const guildId = msg.guild.id;

        const servers = await DatabaseService.instance.rconServer.all(guildId);
        if (servers.length === 0)
        {
            return msg.reply("You do not have any registered servers. Maybe try to add some with the command *!addserver*.");
        }

        // create reply list with header
        const serverEntries: string[] = [];
        serverEntries.push(`You have ${servers.length} servers registered.`);
        for(let i = 0; i < servers.length; i++) {
            const server = servers[i];
            serverEntries.push(`${(i+1)}) ${server.name} @ ${server.ip} **[**RCON Port: ${server.rconPort}**]** **[**Query Port: ${server.queryPort}**]**`);
        }
        serverEntries.push('You can remove servers with the command *!deleteserver* or add additional servers with the command *!addserver*')

        const reply = serverEntries.join('\n');
        return msg.reply(reply, {
            split: { maxLength: 2000 }
        });
    }
};