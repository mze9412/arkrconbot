// Copyright (C) 2020 Mathias Zech
// This file is part of ArkRconBot <https://gitlab.com/mze9412/arkrconbot>.
//
// ArkRconBot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArkRconBot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArkRconBot. If not, see <http://www.gnu.org/licenses/>.

import { Message } from "discord.js";
import { Command, CommandoClient, CommandoMessage } from "discord.js-commando";
import config from '../../config.json'
import { DiscordService } from "../services/discord.service";

export class InformHostedCommand extends Command {
	constructor(client: CommandoClient) {
		super(client, {
			name: 'inform',
			group: 'owner',
			memberName: 'inform',
			description: 'Informs all hosted bot users about something (like downtime) via PM.',
            guildOnly: true,
            args: [
                {
                    key: 'message',
                    prompt: 'What is the message?',
                    type: 'string'
                }
            ]
		});
    }
    
	public async run(msg: CommandoMessage, args: {message: string}): Promise<Message | Message[]> {
        if (msg.member?.id !== config.ownerDiscordId) {
            return msg.reply('Sorry, you cannot use this command.');
        }
        
        // get and iterate all guilds
        const guilds = msg.client?.guilds.cache;
        let messageCount: number = 0; // count how many owners have been informed
        for (const guildEntry of guilds) {
            const guild = guildEntry[1];
            const owner = guild.owner;

            // send message to owner
            if (owner != null && msg.client != null) {
                await DiscordService.instance.sendPrivateMessage(msg.client, owner.id, args.message);
                messageCount++;
            }
        }

        // return message
        return msg.reply(`I have informed ${messageCount} server owners.`, {
            split: { maxLength: 2000 }
        });
    }
};