// Copyright (C) 2020 Mathias Zech
// This file is part of ArkRconBot <https://gitlab.com/mze9412/arkrconbot>.
//
// ArkRconBot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArkRconBot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArkRconBot. If not, see <http://www.gnu.org/licenses/>.

import { Message } from "discord.js";
import { Command, CommandoClient, CommandoMessage } from "discord.js-commando";
import { BlacklistEntry } from "../models/blacklistEntry";
import { DatabaseService } from "../services/database.service";

export class AddBlacklistEntryCommand extends Command {
    constructor(client: CommandoClient) {
        super(client, {
            name: 'addblacklist',
            aliases: ['add-blacklist'],
            group: 'admin',
            memberName: 'addblacklist',
            description: 'Adds the string to the list of blacklist entries.',
            examples: ['!addblacklist saveworld', '!add-blacklist banplayer'],
            userPermissions: ['ADMINISTRATOR'],
            guildOnly: true,
            args: [
                {
                    key: 'blacklist',
                    prompt: 'What is the string you want to blacklist?',
                    type: 'string'
                }
            ]
        });
    }
    
    public async run(msg: CommandoMessage, args: {blacklist: string}): Promise<Message | Message[]> {
        // create role object and store
        const role = new BlacklistEntry();
        role.blacklistString = args.blacklist.toLowerCase();
        role.guildId = msg.guild.id;

        // store
        const success = await DatabaseService.instance.blacklist.store(role);
        if(success) {
            return msg.reply(`The blacklist entry *${args.blacklist}* was successfully added.`);
        } else {
            return msg.reply(`There is already a blacklist entry *${args.blacklist}*.`);
        }
    }
};