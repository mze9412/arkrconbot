// Copyright (C) 2020 Mathias Zech
// This file is part of ArkRconBot <https://gitlab.com/mze9412/arkrconbot>.
//
// ArkRconBot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArkRconBot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArkRconBot. If not, see <http://www.gnu.org/licenses/>.

import { Message } from "discord.js";
import { Command, CommandoClient, CommandoMessage } from "discord.js-commando";
import { DatabaseService } from "../services/database.service";

export class ListBlacklistEntriesCommand extends Command {
	constructor(client: CommandoClient) {
		super(client, {
			name: 'listblacklist',
			aliases: ['list-blacklist'],
			group: 'admin',
			memberName: 'listblacklist',
			description: 'Lists all blacklisted RCON commands.',
            userPermissions: ['ADMINISTRATOR'],
            guildOnly: true
		});
    }
    
	public async run(msg: CommandoMessage): Promise<Message | Message[]> {
        const entries = await DatabaseService.instance.blacklist.all(msg.guild.id);

        // 0 means only admins
        if (entries.length === 0) {
            return msg.reply('You have no blacklist. You can add entries with *!add-blacklist <command>*.')
        }

        // list roles
        const buff: string[] = [];
        buff.push(`You have ${entries.length} blacklist entries.`);
        for (let i = 0; i < entries.length; i++) {
            const entry = entries[i];
            buff.push(`${i+1}) ${entry.blacklistString}`);
        }

        // return text
        return msg.reply(buff.join('\n'), { split: { maxLength: 2000 }})

    }
};