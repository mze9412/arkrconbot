// Copyright (C) 2020 Mathias Zech
// This file is part of ArkRconBot <https://gitlab.com/mze9412/arkrconbot>.
//
// ArkRconBot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArkRconBot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArkRconBot. If not, see <http://www.gnu.org/licenses/>.

import { Message } from "discord.js";
import { CommandoClient, CommandoMessage } from "discord.js-commando";
import { DatabaseService } from "../services/database.service";
import { RconService } from "../services/rcon.service";
import { RconBotCommand } from "./rconBotCommand";

export class RconCommand extends RconBotCommand {
	constructor(client: CommandoClient) {
		super(client, {
			name: 'rcon',
			group: 'rcon',
			memberName: 'rcon',
            description: 'Executes RCON command on the target server.',
            examples: ['!rcon scorchedearth "broadcast Hi everyone!"', '!rcon extinction5 "settimeofday 10:00"'],
            guildOnly: true,
            args: [
                {
                    key: 'name',
                    prompt: 'What is the server\'s name?',
                    type: 'string'
                },
                {
                    key: 'cmd',
                    prompt: 'What is the rcon command to execute?',
                    type: 'string'
                }
            ]
		});
    }
    
	public async run(msg: CommandoMessage, args: {name: string, cmd: string}): Promise<Message | Message[]> {
        if (! await this.hasAccess(msg)) {
            return msg.reply('Sorry, you are not allowed to use this command.');
        }

        const guildId = msg.guild.id;

        const server = await DatabaseService.instance.rconServer.get(guildId, args.name);
        if(server != null) {
            try {
                // execute command
                const result = await RconService.instance.runRconCommand(server, args.cmd);
                const buffer: string[] = [];
                buffer.push(`Command *${args.cmd}* executed on server *${args.name}*.`);
                buffer.push(result);
                return msg.reply(buffer.join('\n'), {
                    split: { maxLength: 2000 }
                });
            } catch(ex) {
                console.error(ex);
                return msg.reply(ex);
            }
        } else {
            return msg.reply(`No server named *${args.name}* exists.`);
        }
    }
};