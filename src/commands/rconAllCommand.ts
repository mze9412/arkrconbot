// Copyright (C) 2020 Mathias Zech
// This file is part of ArkRconBot <https://gitlab.com/mze9412/arkrconbot>.
//
// ArkRconBot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArkRconBot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArkRconBot. If not, see <http://www.gnu.org/licenses/>.

import { Message } from "discord.js";
import { CommandoClient, CommandoMessage } from "discord.js-commando";
import { DatabaseService } from "../services/database.service";
import { RconService } from "../services/rcon.service";
import { RconBotCommand } from "./rconBotCommand";

export class RconAllCommand extends RconBotCommand {
	constructor(client: CommandoClient) {
		super(client, {
			name: 'rcon-all',
			aliases: ['rcon-all'],
			group: 'rcon',
			memberName: 'rconall',
            description: 'Executes RCON command on all stored servers.',
            examples: ['!rconall "broadcast Hi everyone!"', '!rcon-all "settimeofday 10:00"'],
            guildOnly: true,
            args: [
                {
                    key: 'cmd',
                    prompt: 'What is the rcon command to execute?',
                    type: 'string'
                }
            ]
		});
    }
    
	public async run(msg: CommandoMessage, args: {cmd: string}): Promise<Message | Message[]> {
        if (! await this.hasAccess(msg)) {
            return msg.reply('Sorry, you are not allowed to use this command.');
        }

        const guildId = msg.guild.id;

        const servers = await DatabaseService.instance.rconServer.all(guildId);
        if(servers.length === 0) {
            return msg.reply('No servers found to execute the command on. Maybe add some with command *!addserver* first.');
        } else {
            // create result buffer
            const buffer: string[] = [];
            buffer.push(`Command *"${args.cmd}"* executed on *${servers.length}* servers.`);

            // execute command for each server
            for(const server of servers) {
                const result = await RconService.instance.runRconCommand(server, args.cmd);
                buffer.push(`**${server.name}**`);
                buffer.push(result);
                buffer.push('\n');
            }

            return msg.reply(buffer.join('\n'), {
                split: { maxLength: 2000 }
            });
        }
    }
};