// Copyright (C) 2020 Mathias Zech
// This file is part of ArkRconBot <https://gitlab.com/mze9412/arkrconbot>.
//
// ArkRconBot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArkRconBot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArkRconBot. If not, see <http://www.gnu.org/licenses/>.

import { Message } from "discord.js";
import { Command, CommandoClient, CommandoMessage } from "discord.js-commando";
import { RconRole } from "../models/rconRole";
import { DatabaseService } from "../services/database.service";

export class AddRconRoleCommand extends Command {
    constructor(client: CommandoClient) {
        super(client, {
            name: 'addrole',
            aliases: ['add-role'],
            group: 'admin',
            memberName: 'addrole',
            description: 'Adds the named role to the list of allows RCON roles.',
            examples: ['!addrole rcon', '!add-role moderators'],
            userPermissions: ['ADMINISTRATOR'],
            guildOnly: true,
            args: [
                {
                    key: 'name',
                    prompt: 'What is the role\'s name?',
                    type: 'string'
                }
            ]
        });
    }
    
    public async run(msg: CommandoMessage, args: {name: string}): Promise<Message | Message[]> {
        // create role object
        const role = new RconRole();
        role.roleName = args.name;
        role.guildId = msg.guild.id;

        // store
        const success = await DatabaseService.instance.rconRole.store(role);
        if(success) {
            return msg.reply(`The RCON role with the name *${args.name}* was successfully added.`);
        } else {
            return msg.reply(`There is already registered RCON role with the name *${args.name}*.`);
        }
    }
};