// Copyright (C) 2020 Mathias Zech
// This file is part of ArkRconBot <https://gitlab.com/mze9412/arkrconbot>.
//
// ArkRconBot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArkRconBot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArkRconBot. If not, see <http://www.gnu.org/licenses/>.

import { Message } from "discord.js";
import { Command, CommandoClient, CommandoMessage } from "discord.js-commando";
import { DatabaseService } from "../services/database.service";

export class ListRconRolesCommand extends Command {
	constructor(client: CommandoClient) {
		super(client, {
			name: 'listroles',
			aliases: ['list-roles'],
			group: 'admin',
			memberName: 'listroles',
			description: 'Lists all roles which are allowed to run RCON commands on your Discord.',
            userPermissions: ['ADMINISTRATOR'],
            guildOnly: true
		});
    }
    
	public async run(msg: CommandoMessage): Promise<Message | Message[]> {
        const roles = await DatabaseService.instance.rconRole.all(msg.guild.id);

        // 0 means only admins
        if (roles.length === 0) {
            return msg.reply('You have no roles registered. Only members with Administrator permissions on your Discord can use RCON commands.\nUse *!add-role* to add a role to this list.')
        }

        // list roles
        const buff: string[] = [];
        buff.push(`You have ${roles.length} roles registered for RCON commands.`);
        for (let i = 0; i < roles.length; i++) {
            const role = roles[i];
            buff.push(`${i+1}) ${role.roleName}`);
        }

        // return text
        return msg.reply(buff.join('\n'), { split: { maxLength: 2000 }})

    }
};