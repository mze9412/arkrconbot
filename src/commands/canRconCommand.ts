// Copyright (C) 2020 Mathias Zech
// This file is part of ArkRconBot <https://gitlab.com/mze9412/arkrconbot>.
//
// ArkRconBot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArkRconBot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArkRconBot. If not, see <http://www.gnu.org/licenses/>.

import { Message } from "discord.js";
import { CommandoClient, CommandoMessage } from "discord.js-commando";
import { RconBotCommand } from "./rconBotCommand";

export class CanRconCommand extends RconBotCommand {
    constructor(client: CommandoClient) {
        super(client, {
            name: 'canrcon',
            aliases: ['can-rcon'],
            group: 'everyone',
            memberName: 'canrcon',
            description: 'Checks if you are allowed to execute rcon commands.',
            guildOnly: true
        });
    }
    
    public async run(msg: CommandoMessage): Promise<Message | Message[]> {
        if (! await this.hasAccess(msg)) {
            return msg.reply('Sorry, you are not allowed to use RCON commands.');
        }
        return msg.reply('You are allowed to run RCON commands. Congratulations.')
    }
};