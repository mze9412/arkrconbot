// Copyright (C) 2020 Mathias Zech
// This file is part of ArkRconBot <https://gitlab.com/mze9412/arkrconbot>.
//
// ArkRconBot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArkRconBot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArkRconBot. If not, see <http://www.gnu.org/licenses/>.

import { Command, CommandInfo, CommandoClient, CommandoMessage } from "discord.js-commando";
import { DatabaseService } from "../services/database.service";

export abstract class RconBotCommand extends Command {
    
	constructor(client: CommandoClient, info: CommandInfo) {
        super(client, info);
    }

    protected async hasAccess(msg: CommandoMessage): Promise<boolean> {
        const roles = await DatabaseService.instance.rconRole.all(msg.guild.id);

        let hasRole = false;
        let hasAdminPermission = false;
        let isBlacklisted = false;

        // if no roles, then no access
        if (roles.length > 0) {
            for(const role of roles) {
                if (msg.member?.roles.cache.some(r => r.name == role.roleName)) {
                    hasRole = true;
                    break;
                }
            }
        }

        // check for ADMIN permissions
        if (msg.member?.permissions.has('ADMINISTRATOR')) {
            hasAdminPermission = true;
        }

        // if no admin but has the role, check blacklist
        if (hasRole && !hasAdminPermission) {
            // get entries
            const entries = await DatabaseService.instance.blacklist.all(msg.guild.id);

            // only do anything if entries exist
            if (entries.length > 0) {
                // check case insensitive with lower-case string
                const command = msg.content.toLowerCase();

                // compare to all entries
                for (const entry of entries) {
                    if (command.indexOf(entry.blacklistString) != -1) {
                        // first match aborts loop
                        isBlacklisted = true;
                        break;
                    }
                }
            }
        }

        // if user has the role and the entry is not blacklisted OR has admin permissions, return true
        const hasAccess = (hasRole && !isBlacklisted) || hasAdminPermission;
        console.log(`Role: ${hasRole} IsBlackListed: ${isBlacklisted} HasAdmin: ${hasAdminPermission} Total: ${hasAccess}`);
        return hasAccess;
    }
}