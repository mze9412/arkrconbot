// Copyright (C) 2020 Mathias Zech
// This file is part of ArkRconBot <https://gitlab.com/mze9412/arkrconbot>.
//
// ArkRconBot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArkRconBot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArkRconBot. If not, see <http://www.gnu.org/licenses/>.

import { Message } from "discord.js";
import { Command, CommandoClient, CommandoMessage } from "discord.js-commando";
import { RconServer } from "../models/rconServer";
import { DatabaseService } from "../services/database.service";

export class AddServerCommand extends Command {
    constructor(client: CommandoClient) {
        super(client, {
            name: 'addserver',
            aliases: ['add-server'],
            group: 'admin',
            memberName: 'addserver',
            description: 'Adds a server to the list of saved rcon targets.',
            examples: ['!addServer scorchedearth 10.0.0.1 7065 secretPassword', '!addServer'],
            userPermissions: ['ADMINISTRATOR'],
            guildOnly: true,
            args: [
                {
                    key: 'name',
                    prompt: 'What is the server\'s name?',
                    type: 'string'
                },
                {
                    key: 'ip',
                    prompt: 'What is the server\'s hostname or IP address?',
                    type: 'string'
                },
                {
                    key: 'rconPort',
                    prompt: 'What is the server\'s RCON port?',
                    type: 'integer'
                },
                {
                    key: 'queryPort',
                    prompt: 'What is the server\'s query port?',
                    type: 'integer'
                },
                {
                    key: 'password',
                    prompt: 'What is the server\'s RCON password?',
                    type: 'string'
                }
            ]
        });
    }
    
    public async run(msg: CommandoMessage, args: {name: string, ip: string, rconPort: number, queryPort: number, password: string}): Promise<Message | Message[]> {
        const server = new RconServer();
        server.guildId = msg.guild.id;
        server.name = args.name;
        server.ip = args.ip;
        server.rconPort = args.rconPort;
        server.queryPort = args.queryPort;
        server.password = args.password;

        const success = await DatabaseService.instance.rconServer.store(server, false);
        if(success) {
            return msg.reply(`The server with the name *${args.name}* was successfully saved.`);
        } else {
            return msg.reply(`There is already server with the name *${args.name}*.`);
        }
    }
};