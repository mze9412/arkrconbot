// Copyright (C) 2020 Mathias Zech
// This file is part of ArkRconBot <https://gitlab.com/mze9412/arkrconbot>.
//
// ArkRconBot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArkRconBot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArkRconBot. If not, see <http://www.gnu.org/licenses/>.

import { Message } from "discord.js";
import { Command, CommandoClient, CommandoMessage } from "discord.js-commando";
import { DatabaseService } from "../services/database.service";

export class DeleteRconRoleCommand extends Command {
    constructor(client: CommandoClient) {
        super(client, {
            name: 'deleterole',
            aliases: ['delete-role'],
            group: 'admin',
            memberName: 'deleterole',
            description: 'Deletes the named role from the list of allows RCON roles.',
            examples: ['!deleterole rcon', '!delete-role moderators'],
            userPermissions: ['ADMINISTRATOR'],
            guildOnly: true,
            args: [
                {
                    key: 'name',
                    prompt: 'What is the role\'s name?',
                    type: 'string'
                }
            ]
        });
    }
    
    public async run(msg: CommandoMessage, args: {name: string}): Promise<Message | Message[]> {
        const guildId = msg.guild.id;

        // get role
        const role = await DatabaseService.instance.rconRole.get(guildId, args.name);
        if (role == null) {
            return msg.reply(`No RCON role named *${args.name}* exists.`);
        }

        const success = await DatabaseService.instance.rconRole.delete(role);
        if(success) {
            return msg.reply(`The RCON role with the name *${args.name}* was successfully deleted.`);
        } else {
            return msg.reply(`No RCON role named *${args.name}* exists.`);
        }
    }
};