// Copyright (C) 2020 Mathias Zech
// This file is part of ArkRconBot <https://gitlab.com/mze9412/arkrconbot>.
//
// ArkRconBot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArkRconBot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArkRconBot. If not, see <http://www.gnu.org/licenses/>.

import { Message } from "discord.js";
import { Command, CommandoClient, CommandoMessage } from "discord.js-commando";
import { DatabaseService } from "../services/database.service";

export class DeleteServerCommand extends Command {
    constructor(client: CommandoClient) {
        super(client, {
            name: 'deleteserver',
            aliases: ['delete-server'],
            group: 'admin',
            memberName: 'deleteserver',
            description: 'Deletes the named server from the list of saved rcon targets.',
            examples: ['!deleteServer scorchedearth', '!deleteServer extinction5'],
            userPermissions: ['ADMINISTRATOR'],
            guildOnly: true,
            args: [
                {
                    key: 'name',
                    prompt: 'What is the server\'s name?',
                    type: 'string'
                }
            ]
        });
    }
    
    public async run(msg: CommandoMessage, args: {name: string}): Promise<Message | Message[]> {
        const guildId = msg.guild.id;

        // get server
        const server = await DatabaseService.instance.rconServer.get(guildId, args.name);
        if (server == null) {
            return msg.reply(`No server named *${args.name}* exists.`);
        }

        const success = await DatabaseService.instance.rconServer.delete(server);
        if(success) {
            return msg.reply(`The server with the name *${args.name}* was successfully deleted.`);
        } else {
            return msg.reply(`No server named *${args.name}* exists.`);
        }
    }
};