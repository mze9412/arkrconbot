// Copyright (C) 2020 Mathias Zech
// This file is part of ArkRconBot <https://gitlab.com/mze9412/arkrconbot>.
//
// ArkRconBot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArkRconBot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArkRconBot. If not, see <http://www.gnu.org/licenses/>.

import { Message } from "discord.js";
import { Command, CommandoClient, CommandoMessage } from "discord.js-commando";
import config from '../../config.json'

export class StatisticsCommand extends Command {
	constructor(client: CommandoClient) {
		super(client, {
			name: 'statistics',
			group: 'owner',
			memberName: 'statistics',
			description: 'Displays bot statistics.',
            guildOnly: true
		});
    }
    
	public async run(msg: CommandoMessage): Promise<Message | Message[]> {
        if (msg.member?.id !== config.ownerDiscordId) {
            return msg.reply('Sorry, you cannot use this command.');
        }

        const buffer: string[] = ['\n**Discords**'];

        const guilds = msg.client?.guilds.cache;
        for (const guildEntry of guilds) {
            const guild = guildEntry[1];
            const owner = guild.owner;
            buffer.push(`_${guild.name}_ (${guild.id}) owned by _${owner?.displayName}_ (${owner?.id})`);
        }

        buffer.push(`\n**Guilds**: ${msg.client?.guilds.cache.size}`);
        buffer.push(`**Channels**: ${msg.client?.channels.cache.size}`);
        buffer.push(`**Users**: ${msg.client?.users.cache.size}`);
        // TODO maybe some more

        // return message
        return msg.reply(buffer.join('\n'), {
            split: { maxLength: 2000 }
        });
    }
};