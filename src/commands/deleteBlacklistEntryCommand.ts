// Copyright (C) 2020 Mathias Zech
// This file is part of ArkRconBot <https://gitlab.com/mze9412/arkrconbot>.
//
// ArkRconBot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArkRconBot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArkRconBot. If not, see <http://www.gnu.org/licenses/>.

import { Message } from "discord.js";
import { Command, CommandoClient, CommandoMessage } from "discord.js-commando";
import { DatabaseService } from "../services/database.service";

export class DeleteBlacklistEntryCommand extends Command {
    constructor(client: CommandoClient) {
        super(client, {
            name: 'deleteblacklist',
            aliases: ['delete-blacklist'],
            group: 'admin',
            memberName: 'deleteblacklist',
            description: 'Deletes the named blacklist entry from the list of blacklist entries.',
            examples: ['!deleteblacklist saveworld', '!delete-blacklist banplayer'],
            userPermissions: ['ADMINISTRATOR'],
            guildOnly: true,
            args: [
                {
                    key: 'blacklist',
                    prompt: 'What is the string you want to remove from the blacklist?',
                    type: 'string'
                }
            ]
        });
    }
    
    public async run(msg: CommandoMessage, args: {blacklist: string}): Promise<Message | Message[]> {
        const guildId = msg.guild.id;

        // get entry
        const entry = await DatabaseService.instance.blacklist.get(guildId, args.blacklist);
        if (entry == null) {
            return msg.reply(`No blacklist entry *${args.blacklist}* exists.`);
        }

        const success = await DatabaseService.instance.blacklist.delete(entry);
        if(success) {
            return msg.reply(`The blacklist entry *${args.blacklist}* was successfully deleted.`);
        } else {
            return msg.reply(`No blacklist entry *${args.blacklist}* exists.`);
        }
    }
};