// Copyright (C) 2020 Mathias Zech
// This file is part of ArkRconBot <https://gitlab.com/mze9412/arkrconbot>.
//
// ArkRconBot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArkRconBot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArkRconBot. If not, see <http://www.gnu.org/licenses/>.

import { Message } from "discord.js";
import { Command, CommandoClient, CommandoMessage } from "discord.js-commando";
import { BotConfig } from "../models/botConfig";
import { DatabaseService } from "../services/database.service";

export class ConfigureHostedChatLogCommand extends Command {
    constructor(client: CommandoClient) {
        super(client, {
            name: 'configure-chatlog',
            group: 'admin',
            memberName: 'configure-chatlog',
            description: 'Allows you to configure the chatlog feature.',
            examples: ['!configure-chatlog', '!configure-chatlog Y'],
            userPermissions: ['ADMINISTRATOR'],
            guildOnly: true,
            args: [
                {
                    key: 'logServerChat',
                    prompt: 'Do you want to enable server chat to discord logging?\nOnly one bot/client can read ingame chat via RCON. Do not enable this feature, if you are using CrossArkChat plugins. Y/N',
                    type: 'string'
                }
            ]
        });
    }
    
    public async run(msg: CommandoMessage, args: {logServerChat: string}): Promise<Message | Message[]> {
        const guildId = msg.guild.id;

        let config = await DatabaseService.instance.botConfig.get(guildId);
        if (config == null) {
            config = new BotConfig();
            config.guildId = guildId;
        }

        config.logServerChat = args.logServerChat === 'Y' || args.logServerChat === 'y';

        const success = await DatabaseService.instance.botConfig.store(config);
        if(success) {
            return msg.reply('Configuration successfully saved.');
        } else {
            return msg.reply('There was an error with your configuration. Please try again.');
        }
    }
};