// Copyright (C) 2020 Mathias Zech
// This file is part of ArkRconBot <https://gitlab.com/mze9412/arkrconbot>.
//
// ArkRconBot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArkRconBot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArkRconBot. If not, see <http://www.gnu.org/licenses/>.

import { Message } from "discord.js";
import { Command, CommandoClient, CommandoMessage } from "discord.js-commando";
import { DatabaseService } from "../services/database.service";
export class PrintConfigurationCommand extends Command {
    constructor(client: CommandoClient) {
        super(client, {
            name: 'printconfiguration',
            aliases: ['print-configuration, printconfig, print-config'],
            group: 'admin',
            memberName: 'printconfiguration',
            description: 'Prints current bot configuration.',
            userPermissions: ['ADMINISTRATOR'],
            guildOnly: true
        });
    }
    
    public async run(msg: CommandoMessage): Promise<Message | Message[]> {
        const guildId = msg.guild.id;
        const conf = await DatabaseService.instance.botConfig.get(guildId);
        if (conf == null) {
            return msg.reply('You did not apply any configuration, yet. Run *!configure*, please.');
        } else {
            const buff: string[] = [];

            // header
            buff.push('**Bot Configuration**');

            // server status
            buff.push(`**[** Show Server Status: *${conf.showServerStatus ? 'Yes' : 'No'}* **]**`);
            buff.push(`**[** Allow joining of status channels: *${conf.allowStatusChannelJoin ? 'Yes' : 'No'}* **]**`);
            buff.push(`**[** Get Server Chat: *${conf.logServerChat ? 'Yes' : 'No'}* **]**`);

            // return message
            return msg.reply(buff.join('\n'), {
                split: { maxLength: 2000 }
            });
        }
    }
};