// Copyright (C) 2020 Mathias Zech
// This file is part of ArkRconBot <https://gitlab.com/mze9412/arkrconbot>.
//
// ArkRconBot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArkRconBot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArkRconBot. If not, see <http://www.gnu.org/licenses/>.

import { BotConfig } from "../models/botConfig";
import { ServerInfo } from "../models/serverInfo";
import { DatabaseService } from "./database.service";
import axios from 'axios';
import { RconService } from "./rcon.service";



export class ServerDataService {
    private static _instance: ServerDataService;

    public static get instance(): ServerDataService {
        if (this._instance == null) {
            this._instance = new ServerDataService();
        }
        return this._instance;
    }

    private constructor() {}

    public async getServerData(config: BotConfig): Promise<ServerInfo[]> {
        const servers = await DatabaseService.instance.rconServer.all(config.guildId);

        // result list
        const infos: ServerInfo[] = [];

        // get data for all servers
        for (const server of servers) {
            try {
                //read data 
                const url = `https://api.7nz.de/ark/json/server-info/${server.ip}:${server.queryPort}`;
                const result = await axios.get(url);
                
                let jsonData = undefined;
                if (result.status === 200) {
                    jsonData = result.data;
                }
                // use JSON data
                const info = new ServerInfo();
                info.server = server; // store server data
                info.online = jsonData.online;
                if (info.online) {
                    info.name = server.name;
                    info.maxPlayers = jsonData.max_players;

                    if (jsonData.players > 0) {
                        const list = await RconService.instance.getPlayerList(server);
                        info.players = list[0];
                        info.playersNoSteam = list[1];
                        info.playerCount = info.players.length;
                    }
                } else {
                    info.name = server.name + ' - Offline';
                    info.maxPlayers = -1;
                    info.playerCount = -1;
                }

                // add to result list
                infos.push(info);
            } catch(ex) {
                const info = new ServerInfo();
                info.name = server.name + '- Exception';
                info.maxPlayers = -1;
                info.maxPlayers = -1;
                infos.push(info);
            }
        }

        return infos;
    }
}