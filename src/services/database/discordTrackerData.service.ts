// Copyright (C) 2020 Mathias Zech
// This file is part of ArcRconBot <https://gitlab.com/mze9412/ArcRconBot>.
//
// ArcRconBot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArcRconBot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArcRconBot. If not, see <http://www.gnu.org/licenses/>.

import { DiscordTracker, DiscordTrackerType } from "../../models/discordTracker";
import { DatabaseServiceBase } from "./databaseServiceBase";

export class DiscordTrackerDataService extends DatabaseServiceBase<DiscordTracker> {
    public constructor(url: string, user: string, password: string, authSource: string, databaseName: string) {
        super(url, user, password, authSource, databaseName, 'discordTrackers');
    }
    
    public async store(data: DiscordTracker, overwriteIfExists: boolean = true): Promise<boolean> {
        // delete if it exists or return false if overwrite is not desired
        const exists = await this.exists(data);
        if(overwriteIfExists && exists) {
            await this.delete(data);
        } else if(exists) {
            return false;
        }

        const client = await this.connect();
        const db = await this.openDatabase(client);
        const collection = db.collection<DiscordTracker>(this.collectionName);
        const insertResult = await collection.insertOne(data);
        await client.close();
        
        return insertResult.insertedCount === 1;
    }

    public async exists(data: DiscordTracker): Promise<boolean> {
        return (await this.get(data.guildId, data.name, data.trackerType)) != null;
    }

    public async delete(data: DiscordTracker): Promise<number> {
        const client = await this.connect();
        const db = await this.openDatabase(client);
        const collection = db.collection<DiscordTracker>(this.collectionName);
        const deleteResult = await collection.deleteOne({guildId: data.guildId, name: data.name});
        await client.close();
        
        return deleteResult.deletedCount == undefined ? 0 : <number>deleteResult.deletedCount;
    }

    public async deleteAll(guildId: string): Promise<number> {
        const client = await this.connect();
        const db = await this.openDatabase(client);
        const collection = db.collection<DiscordTracker>(this.collectionName);
        const deleteResult = await collection.deleteMany({guildId: guildId});
        await client.close();
        
        return deleteResult.deletedCount == undefined ? 0 : <number>deleteResult.deletedCount;
    }

    public async get(guildId: string, name: string, type: DiscordTrackerType): Promise<DiscordTracker | null> {
        const client = await this.connect();
        const db = await this.openDatabase(client);
        const collection = db.collection<DiscordTracker>(this.collectionName);
        const result = await collection.findOne({ guildId: guildId, name: name, trackerType: type });
        await client.close();
        return result;
    }

    public async all(guildId: string): Promise<DiscordTracker[]> {
        const client = await this.connect();
        const db = await this.openDatabase(client);
        const collection = db.collection<DiscordTracker>(this.collectionName);
        const result = await collection.find({ guildId: guildId }).toArray();
        await client.close();
        return result;
    }

    public async group(guildId: string, trackerGroup: string): Promise<DiscordTracker[]> {
        const client = await this.connect();
        const db = await this.openDatabase(client);
        const collection = db.collection<DiscordTracker>(this.collectionName);
        const result = await collection.find({ guildId: guildId, group: trackerGroup }).toArray();
        await client.close();
        return result;
    }
}