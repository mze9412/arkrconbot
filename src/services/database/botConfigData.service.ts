// Copyright (C) 2020 Mathias Zech
// This file is part of ArcRconBot <https://gitlab.com/mze9412/ArcRconBot>.
//
// ArcRconBot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArcRconBot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArcRconBot. If not, see <http://www.gnu.org/licenses/>.

import { BotConfig } from "../../models/botConfig";
import { DatabaseServiceBase } from "./databaseServiceBase";

export class BotConfigDataService extends DatabaseServiceBase<BotConfig> {
    public constructor(url: string, user: string, password: string, authSource: string, databaseName: string) {
        super(url, user, password, authSource, databaseName, 'botConfigs');
    }
    
    public async store(data: BotConfig, overwriteIfExists: boolean = true): Promise<boolean> {
        // delete if it exists or return false if overwrite is not desired
        const exists = await this.exists(data);
        if(overwriteIfExists && exists) {
            await this.delete(data);
        } else if(exists) {
            return false;
        }

        const client = await this.connect();
        const db = await this.openDatabase(client);
        const collection = db.collection<BotConfig>(this.collectionName);
        const insertResult = await collection.insertOne(data);
        await client.close();
        
        return insertResult.insertedCount === 1;
    }

    public async exists(data: BotConfig): Promise<boolean> {
        return (await this.get(data.guildId)) != null;
    }

    public async delete(data: BotConfig): Promise<number> {
        const client = await this.connect();
        const db = await this.openDatabase(client);
        const collection = db.collection<BotConfig>(this.collectionName);
        const deleteResult = await collection.deleteOne({guildId: data.guildId});
        await client.close();
        
        return deleteResult.deletedCount == undefined ? 0 : <number>deleteResult.deletedCount;
    }

    public async deleteAll(guildId: string): Promise<number> {
        const client = await this.connect();
        const db = await this.openDatabase(client);
        const collection = db.collection<BotConfig>(this.collectionName);
        const deleteResult = await collection.deleteMany({guildId: guildId});
        await client.close();
        
        return deleteResult.deletedCount == undefined ? 0 : <number>deleteResult.deletedCount;
    }

    public async get(guildId: string): Promise<BotConfig | null> {
        const client = await this.connect();
        const db = await this.openDatabase(client);
        const collection = db.collection<BotConfig>(this.collectionName);
        const result = await collection.findOne({ guildId: guildId });
        await client.close();
        return result;
    }

    public async all(): Promise<BotConfig[]> {
        const client = await this.connect();
        const db = await this.openDatabase(client);
        const collection = db.collection<BotConfig>(this.collectionName);
        const result = await collection.find({}).toArray();
        await client.close();
        return result;
    }
}