// Copyright (C) 2020 Mathias Zech
// This file is part of ArcRconBot <https://gitlab.com/mze9412/ArcRconBot>.
//
// ArcRconBot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArcRconBot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArcRconBot. If not, see <http://www.gnu.org/licenses/>.


import { BlacklistEntry } from "../../models/blacklistEntry";
import { DatabaseServiceBase } from "./databaseServiceBase";

export class BlacklistEntryDataService extends DatabaseServiceBase<BlacklistEntry> {
    public constructor(url: string, user: string, password: string, authSource: string, databaseName: string) {
        super(url, user, password, authSource, databaseName, 'blacklistEntries');
    }
    
    public async store(data: BlacklistEntry, overwriteIfExists: boolean = true): Promise<boolean> {
        // delete if it exists or return false if overwrite is not desired
        const exists = await this.exists(data);
        if(overwriteIfExists && exists) {
            await this.delete(data);
        } else if(exists) {
            return false;
        }

        const client = await this.connect();
        const db = await this.openDatabase(client);
        const collection = db.collection<BlacklistEntry>(this.collectionName);
        const insertResult = await collection.insertOne(data);
        await client.close();
        
        return insertResult.insertedCount === 1;
    }

    public async exists(data: BlacklistEntry): Promise<boolean> {
        return (await this.get(data.guildId, data.blacklistString)) != null;
    }

    public async delete(data: BlacklistEntry): Promise<number> {
        const client = await this.connect();
        const db = await this.openDatabase(client);
        const collection = db.collection<BlacklistEntry>(this.collectionName);
        const deleteResult = await collection.deleteOne({guildId: data.guildId, blacklistString: data.blacklistString});
        await client.close();
        
        return deleteResult.deletedCount == undefined ? 0 : <number>deleteResult.deletedCount;
    }

    public async deleteAll(guildId: string): Promise<number> {
        const client = await this.connect();
        const db = await this.openDatabase(client);
        const collection = db.collection<BlacklistEntry>(this.collectionName);
        const deleteResult = await collection.deleteMany({guildId: guildId});
        await client.close();
        
        return deleteResult.deletedCount == undefined ? 0 : <number>deleteResult.deletedCount;
    }

    public async get(guildId: string, blacklistString: string): Promise<BlacklistEntry | null> {
        const client = await this.connect();
        const db = await this.openDatabase(client);
        const collection = db.collection<BlacklistEntry>(this.collectionName);
        const result = await collection.findOne({ guildId: guildId, blacklistString: blacklistString });
        await client.close();
        return result;
    }

    public async all(guildId: string): Promise<BlacklistEntry[]> {
        const client = await this.connect();
        const db = await this.openDatabase(client);
        const collection = db.collection<BlacklistEntry>(this.collectionName);
        const result = collection.find({ guildId: guildId }).toArray();
        await client.close();
        return result;
    }
}