// Copyright (C) 2020 Mathias Zech
// This file is part of ArcRconBot <https://gitlab.com/mze9412/ArcRconBot>.
//
// ArcRconBot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArcRconBot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArcRconBot. If not, see <http://www.gnu.org/licenses/>.

import { Db, MongoClient } from "mongodb";

export abstract class DatabaseServiceBase<T> {
    private _databaseUrl: string;
    private _user: string;
    private _password: string;
    private _authSource: string;
    private _databaseName: string;
    private _collectionName: string;

    protected constructor(url: string, user: string, password: string, authSource: string, databaseName: string, collectionName: string) {
        this._databaseUrl = url;
        this._user = user;
        this._password = password;
        this._authSource = authSource;
        this._databaseName = databaseName;
        this._collectionName = collectionName;
    }

    protected async connect(): Promise<MongoClient> {
        return await MongoClient.connect(`mongodb://${this._user}:${this._password}@${this._databaseUrl}?authMechanism=SCRAM-SHA-256&authSource=${this._authSource}`);
    }

    protected async openDatabase(client: MongoClient): Promise<Db> {
        return client.db(this._databaseName);
    }

    public abstract store(data: T, overwriteIfExists: boolean): Promise<boolean>;
    public abstract exists(data: T): Promise<boolean>;
    public abstract delete(data: T): Promise<number>;
    public abstract deleteAll(guildId: string): Promise<number>;

    protected get collectionName(): string { return this._collectionName; }
}