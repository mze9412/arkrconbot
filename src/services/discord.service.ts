// Copyright (C) 2020 Mathias Zech
// This file is part of ArkRconBot <https://gitlab.com/mze9412/ArkRconBot>.
//
// ArkRconBot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArkRconBot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArkRconBot. If not, see <http://www.gnu.org/licenses/>.

import config from '../../config.json';
import { CategoryChannel, Channel, Client, Guild, Message, MessageEmbed, TextChannel, VoiceChannel } from "discord.js";
import { CommandoClient } from "discord.js-commando";
import { Colors } from "../helpers/colors";
import { sleep } from "../helpers/waiting";
import { BotConfig } from "../models/botConfig";
import { DiscordTracker, DiscordTrackerType } from "../models/discordTracker";
import { ServerInfo } from "../models/serverInfo";
import { DatabaseService } from "./database.service";
import { ServerDataService } from "./serverData.service";
import { RconService } from './rcon.service';

export class DiscordService {
    private static _instance: DiscordService;


    public static get instance(): DiscordService {
        if (this._instance == null) {
            this._instance = new DiscordService();
        }
        return this._instance;
    }

    private static UpdateMethod_Recreate = "RECREATE";
    private static UpdateMethod_Edit = "EDIT";

    private constructor() {}
    
    public async runUpdateServerStatus(client: CommandoClient): Promise<void> {
        console.info('Server Info Update Check: Start');
        const configs = await DatabaseService.instance.botConfig.all();
        for (const conf of configs) {
            if (config.isTesting && conf.guildId !== '759054260523761675') {
                continue;
            }
            
            if (conf.showServerStatus) {
                // get infos only if channel is configured and exists
                const infos = await ServerDataService.instance.getServerData(conf);

                // iterate through infos
                let updateCount = 0; // used for throttling
                for (const info of infos) {
                    // decide string
                    let statusString = '';
                    if (info.online) {
                        statusString = `${info.playerCount === 0 ? '💙' : '💚'}${info.name} - ${info.playerCount}/${info.maxPlayers}`
                    } else {
                        statusString = `💔${info.name}`;
                    }
                    console.info(`Status: ${statusString}`);

                    // update channel
                    await this.updateServerInfoChannel(client, conf, info, statusString);
                    updateCount++;

                    // sleep 3 seconds to avoid rate limiting
                    if (updateCount % 3 === 0) {
                        await sleep(3000);
                    }
                }

                // also run player listing
                await this.updatePlayerListing(client, conf, infos);
            }
        }
        console.info('Server Info Update Check: Stop');
    }

    private async updatePlayerListing(client: CommandoClient, conf: BotConfig, info: ServerInfo[]): Promise<void> {
        // cancel execution of not configured
        if (!conf.showPlayerList) {
            return;
        }

        let countPlayersTotal = 0;
        let countOnlineServers = 0;
        for (const i of info) {            
            // decide string and color and count
            let statusString = '';
            let color = Colors.blue;
            if (i.online) {
                countOnlineServers++;
                countPlayersTotal += i.playerCount;
                statusString = `${i.playerCount === 0 ? '💙' : '💚'}${i.name} - ${i.playerCount}/${i.maxPlayers}`
                if (i.playerCount > 0) color = Colors.green;
            } else {
                color = Colors.red;
                statusString = `💔${i.name}`;
            }

            // create players string
            let players = i.players.join('\n');
            if (players.length === 0) {
                players = '-\n\u200b';
            } else {
                players = players + '\n\u200b';
            }

            // if player text is too long, try again without steam IDs
            if (players.length > 1024) { // 1024 is the limit for field values according to https://discordjs.guide/popular-topics/embeds.html#resending-a-received-embed
                players = i.playersNoSteam.join('\n') + '\n\u200b';
            }

            // create specific embed message
            const listMsg = new MessageEmbed()
            .setAuthor(statusString, 'https://gitlab.com/mze9412/arkrconbot/-/raw/main/ArkRconBot.png', 'https://gitlab.com/mze9412/arkrconbot')
            .setTimestamp()
            .setFooter('Brought to you by ArkRconBot - RCON for the masses!')
            .addField('Players', players)
            .setColor(color);

            // send message
            await this.sendEmbedMessage(client, conf.guildId, 'STATUS', '👥playerlist', listMsg, `playerlist_message_${i.server.name}`, 'playerlist', false);
            await sleep(2000);
        }

        // select overview color
        let color = Colors.blue;
        if (countOnlineServers === 0) {
            color = Colors.red;
        } else if (countOnlineServers === info.length) {
            color = Colors.green;
        }


        // create general message and specific messages for each server
        const overViewListMsg = new MessageEmbed()
            .setAuthor('Overview', 'https://gitlab.com/mze9412/arkrconbot/-/raw/main/ArkRconBot.png', 'https://gitlab.com/mze9412/arkrconbot')
            .setTimestamp()
            .setFooter('Brought to you by ArkRconBot - RCON for the masses!')
            .addField('Players Total', `${countPlayersTotal}`)
            .addField('Servers Online', `${countOnlineServers}/${info.length}`)
            .setColor(color);

        // send message
        await this.sendEmbedMessage(client, conf.guildId, 'STATUS', '👥playerlist', overViewListMsg, 'playerlist_message_overview', 'playerlist', false);
    }

    private async updateServerInfoChannel(client: CommandoClient, conf: BotConfig, info: ServerInfo, statusString: string): Promise<void> {
        if (config.features.serverInfoUpdate.method === DiscordService.UpdateMethod_Recreate) {
            await this.updateServerInfoRecreateChannel(client, conf, info, statusString);
        } else if (config.features.serverInfoUpdate.method === DiscordService.UpdateMethod_Edit) {
            await this.updateServerInfoEditChannel(client, conf, info, statusString);
        }
    }

    private async updateServerInfoEditChannel(client: CommandoClient, conf: BotConfig, info: ServerInfo, statusString: string): Promise<void> {
        const channel = <VoiceChannel>await this.getOrCreateChannel(client, info.server.guildId, 'STATUS', info.server.name, 'voice', true, true);

        if (channel != undefined && channel.name !== statusString) {
            await channel.setName(statusString);
        }
    }

    private async updateServerInfoRecreateChannel(client: CommandoClient, conf: BotConfig, info: ServerInfo, statusString: string): Promise<void> {
        // get tracker, delete channel and tracker if they exist
        const oldTracker = await DatabaseService.instance.discordTracker.get(conf.guildId, info.server.name, DiscordTrackerType.Channel);
        if (oldTracker != null) {
            const oldChannel = await this.getChannelByTracker(client, conf.guildId, oldTracker.name);
            if (oldChannel != null) {
                await oldChannel.delete();
            }
            await DatabaseService.instance.discordTracker.delete(oldTracker);
        }

        // create channel and set correct name
        const channel = <VoiceChannel>await this.getOrCreateChannel(client, info.server.guildId, 'STATUS', info.server.name, 'voice', true, true);
        if (channel.name !== statusString) {
            await channel.setName(statusString);
        }
    }

    public async runUpdateChatLog(client: CommandoClient): Promise<void> {
        console.info('Server Chat Update: Start');
        const configs = await DatabaseService.instance.botConfig.all();
        for (const conf of configs) {
            if (config.isTesting && conf.guildId !== '759054260523761675') {
                continue;
            }

            if (conf.logServerChat) {
                const servers = await DatabaseService.instance.rconServer.all(conf.guildId);

                // create / get admin log channel if enabled
                const adminLogChannelName = "adminlog";
                let adminLogChannel: TextChannel | undefined = undefined;
                const adminLogBuffer: string[] = [];
                if (config.features.serverChat.seperateAdminLog) {
                    adminLogChannel = <TextChannel> await this.getOrCreateChannel(client, conf.guildId, 'ServerChat', adminLogChannelName, 'text', true, true);
                }

                // create / get tribe log channel if enabled
                const tribeLogChannelName = "tribelog";
                let tribeLogChannel: TextChannel | undefined = undefined;
                const tribeLogBuffer: string[] = [];
                if (config.features.serverChat.seperateAdminLog) {
                    tribeLogChannel = <TextChannel> await this.getOrCreateChannel(client, conf.guildId, 'ServerChat', tribeLogChannelName, 'text', true, true);
                }

                // iterate through servers
                let numberOfMessages = 0; // used for throttling
                for(const server of servers) {
                    const chatString = await RconService.instance.runRconCommand(server, "getChat");

                    // divide at \n (newline) and parse
                    const chatLines = (await chatString).split('\n');

                    // get channel if chat lines is not empty
                    if (chatLines.length > 0) {
                        const channelName = server.name.replace(' ', '_').toLowerCase();
                        let channel = <TextChannel>client.guilds.cache.find(x => x.id === conf.guildId)?.channels.cache.find(x => x.name === channelName);
                        if (channel == null || channel == undefined) {
                            // create channel
                            channel = <TextChannel> await this.getOrCreateChannel(client, conf.guildId, 'ServerChat', channelName, 'text', true, true);
                        }

                        // parse each line and format for posting to discord channel
                        const buffer: string[] = [];
                        for (const rawLine of chatLines) {

                            const line = await this.parseAndFormatChatLine(server.name, rawLine);
                            // skip empty lines
                            if (line === '') {
                                continue;
                            }

                            if (config.features.serverChat.seperateAdminLog && line.startsWith('**ADMIN**')) {
                                adminLogBuffer.push(line);
                            } else if (config.features.serverChat.seperateTribeLog && line.startsWith('**TRIBE**')) {
                                tribeLogBuffer.push(line);
                            } else {
                                buffer.push(line);
                            }
                        }
                        
                        // send
                        if (buffer.length > 0) {
                            await channel.send(buffer.join('\n'), {
                                split: { maxLength: 2000 }
                            });

                            numberOfMessages++;
                            if (numberOfMessages % 4 === 0) {
                                sleep(5000);
                            }
                        }
                    }
                }

                // push admin log
                if (adminLogChannel != undefined && adminLogBuffer.length > 0) {
                    await adminLogChannel.send(adminLogBuffer.join('\n'), {
                        split: { maxLength: 2000 }
                    });
                    sleep(1000);
                }

                // push tribe log
                if (tribeLogChannel != undefined && tribeLogBuffer.length > 0) {
                    await tribeLogChannel.send(tribeLogBuffer.join('\n'), {
                        split: { maxLength: 2000 }
                    });
                    sleep(1000);
                }
            }
        }
        console.info('Server Chat Update: Stop');
    }

    private async parseAndFormatChatLine(mapName: string, rawLine: string): Promise<string> {
        // excludes lines starting with the following
        const chatExcludeLines = [
            'Server received, But no response!!',
            'An error occured during execution of the RCON command on server'
        ];
        for (const exclude of chatExcludeLines) {
            // if line is to be excluded, return empty string
            if (rawLine.startsWith(exclude)) {
                return '';
            }
        }

        // trim
        rawLine = rawLine.trim();
        if (rawLine === '') {
            return '';
        }

        // deal with types of messages
        if (rawLine.startsWith('AdminCmd: ')) {
            return `**ADMIN** ${rawLine.substring(10)}`;
        } else if (rawLine.startsWith('Tribe ')) {
            // remove Tribe heading
            const line = rawLine.substring(6);
            
            // remove RichColor parts and replace with * for cursive
            const regex = /<RichColor Color="(.*)">(.*)<\/>/;
            const result = regex.exec(line);
            if (result != undefined && result.length === 3) {                
                const part1 = line.substring(0, line.indexOf('<Rich'));
                const part2 = result[2];
                return `**TRIBE** (${mapName}) ${part1}${part2}`;
            }

            // done
            return `**TRIBE** ${line}`;
        } else {
            // this is regular chat
            const nameIndex = rawLine.indexOf(':');
            const name = rawLine.substring(0, nameIndex);
            const message = rawLine.substring(nameIndex+1);

            return `**${name}:** ${message.trim()}`;
        }
    }

    public async sendPrivateMessage(client: Client, ownerDiscordId: string, msg: string): Promise<void> {
        const user = client.users.cache.find(x => x.id === ownerDiscordId);
        if (user != undefined) {
            await user.send(msg, {
                split: { maxLength: 2000 }
            });
        }
    }

    public async sendTextMessage(client: CommandoClient, guildId: string, categoryName: string, channelName: string, msg: string): Promise<void> {
        // get channel
        const channel = <TextChannel> await this.getOrCreateChannel(client, guildId, categoryName,channelName, 'text', true, true);

        // return if channel not found
        if (channel == undefined) {
            return;
        }
        
        // send message
        channel.send(msg, {
            split: { maxLength: 2000 }
        });
    }

    public async sendEmbedMessage(client: CommandoClient, guildId: string, categoryName: string, channelName: string, embedMsg: MessageEmbed, trackerName: string = '', trackerGroup = '', deleteExistingMessage: boolean = false): Promise<void> {
        // get channel
        const channel = <TextChannel> await this.getOrCreateChannel(client, guildId, categoryName, channelName, 'text', true, true);

        // try to get message based on tracker if one is defined
        if (trackerName !== '') {
            const msg = await this.getMessageByTracker(client, guildId, trackerName, channel);
            if (msg != undefined) {
                if (deleteExistingMessage) {
                    await msg.delete();
                    const msgTracker = await DatabaseService.instance.discordTracker.get(guildId, trackerName, DiscordTrackerType.Message);
                    if (msgTracker != null) await DatabaseService.instance.discordTracker.delete(msgTracker);
                } else {
                    await msg.edit(embedMsg);
                    return;
                }
            }
        }
        
        // send new message
        const createdMsg = await channel.send(embedMsg);
        const newMessageTracker = new DiscordTracker();
        newMessageTracker.guildId = guildId;
        newMessageTracker.name = trackerName;
        newMessageTracker.group = trackerGroup;
        newMessageTracker.trackerType = DiscordTrackerType.Message;
        newMessageTracker.trackingId = createdMsg.id;
        await DatabaseService.instance.discordTracker.store(newMessageTracker);
    }

    public async deleteMessageByTracker(client: CommandoClient, guildId: string, trackerName: string, channelName: string): Promise<void> {
        // get channel
        const channel = <TextChannel> await this.getChannelByTracker(client, guildId, channelName);
        if (channel == undefined) {
            return;
        }

        // get message
        const message = await this.getMessageByTracker(client, guildId, trackerName, channel);
        const tracker = await DatabaseService.instance.discordTracker.get(guildId, trackerName, DiscordTrackerType.Message);
        if (tracker != null) {
            // delete tracker
            await DatabaseService.instance.discordTracker.delete(tracker);
        }
        if (message == undefined) {
            return;
        }

        // delete message
        if (message.deletable) {
            await message.delete();
        }
    }

    private async getOrCreateChannel(client: CommandoClient, guildId: string, categoryName: string, channelName: string, channelType: string, adminOnly: boolean, createTracker: boolean): Promise<Channel | undefined> {
        // get guild
        const guild = await this.getGuild(client, guildId);
        if (guild == undefined || !guild.available) {
            return undefined;
        }

        // get or create category, abort if undefined
        const category = await this.getOrCreateCategory(client, guildId, categoryName, createTracker);
        if (category == undefined) {
            return undefined;
        }

        // get channel, return if it exists
        let channel = await this.getChannelByTracker(client, guildId, channelName);
        if (channel != undefined) {
            return channel;
        }

        // try again by name, if found, create tracker for it
        channel = guild.channels.cache.find(x => x.name === channelName);
        if (channel != undefined) {
            const tracker = new DiscordTracker();
            tracker.guildId = guildId;
            tracker.trackerType = DiscordTrackerType.Channel;
            tracker.name = channelName;
            tracker.trackingId = channel.id;
            await DatabaseService.instance.discordTracker.store(tracker, true);

            return channel;
        }

        // create channel and configure
        if (channelType === 'voice') {
            const newChannel = <VoiceChannel> await guild.channels.create(channelName, { parent: category, type: 'voice'});
            await newChannel.updateOverwrite(newChannel.guild.roles.everyone, { CONNECT: !adminOnly });

            const tracker = new DiscordTracker();
            tracker.guildId = guildId;
            tracker.trackerType = DiscordTrackerType.Channel;
            tracker.name = channelName;
            tracker.trackingId = newChannel.id;
            await DatabaseService.instance.discordTracker.store(tracker, true);

            return newChannel;
        } else if (channelType === 'text') {
            const newChannel = <TextChannel> await guild.channels.create(channelName, { parent: category, type: 'text' });
            await newChannel.updateOverwrite(newChannel.guild.roles.everyone, { VIEW_CHANNEL: !adminOnly ,READ_MESSAGE_HISTORY: !adminOnly, SEND_MESSAGES: !adminOnly });
            
            const tracker = new DiscordTracker();
            tracker.guildId = guildId;
            tracker.trackerType = DiscordTrackerType.Channel;
            tracker.name = channelName;
            tracker.trackingId = newChannel.id;
            await DatabaseService.instance.discordTracker.store(tracker, true);

            return newChannel;
        }
    }

    private async getOrCreateCategory(client: CommandoClient, guildId: string, categoryName: string, createTracker: boolean): Promise<CategoryChannel | undefined> {
        // get guild
        const guild = await this.getGuild(client, guildId);
        if (guild == undefined || !guild.available) {
            return undefined;
        }

        // first try via tracker
        let category = await this.getCategoryByTracker(client, guildId, categoryName);
        if (category != undefined) {
            return category;
        }

        // get category, return if exists
        category = await this.getCategoryByName(client, guildId, categoryName);
        if (category != undefined) {
            return category;
        }

        // create new category if it does not exist
        const newCategory = await guild.channels.create(categoryName, { type: 'category'});
        if (createTracker) {
            const tracker = new DiscordTracker();
            tracker.guildId = guildId;
            tracker.trackerType = DiscordTrackerType.Category;
            tracker.name = categoryName;
            tracker.trackingId = newCategory.id;

            await DatabaseService.instance.discordTracker.store(tracker, true);
        }

        return newCategory;
    }

    private async getChannelByTracker(client: CommandoClient, guildId: string, trackerName: string): Promise<Channel | undefined> {
        const tracker = await DatabaseService.instance.discordTracker.get(guildId, trackerName, DiscordTrackerType.Channel);
        if (tracker == undefined) {
            return undefined;
        }

        const guild = await this.getGuild(client, tracker.guildId);
        if (guild == undefined || !guild.available) {
            return;
        }

        const channel = guild.channels.cache.find(x => x.id == tracker.trackingId);
        if (channel == undefined) {
            // delete tracker
            await DatabaseService.instance.discordTracker.delete(tracker);
        }

        return channel;
    }

    private async getMessageByTracker(client: CommandoClient, guildId: string, trackerName: string, channel: TextChannel): Promise<Message | undefined> {
        // get tracker
        const tracker = await DatabaseService.instance.discordTracker.get(guildId, trackerName, DiscordTrackerType.Message);
        if (tracker == undefined) {
            return undefined;
        }

        // get guild
        const guild = await this.getGuild(client, tracker.guildId);
        if (guild == undefined || !guild.available) {
            return;
        }
        
        // get message
        let msg: Message | undefined = undefined;        
        try {
            msg = await channel.messages.fetch(tracker.trackingId);
        } catch(ex) {
            console.log(JSON.stringify(ex));
        };

        // Delete tracker if it is not valid anymore
        if (msg == undefined) {
            await DatabaseService.instance.discordTracker.delete(tracker);
        }
        return msg;
    }

    private async getCategoryByTracker(client: CommandoClient, guildId: string, trackerName: string): Promise<CategoryChannel | undefined> {
        const tracker = await DatabaseService.instance.discordTracker.get(guildId, trackerName, DiscordTrackerType.Category);
        if (tracker == undefined) {
            return undefined;
        }

        const guild = await this.getGuild(client, tracker.guildId);
        if (guild == undefined || !guild.available) {
            return;
        }

        const channel = guild.channels.cache.find(x => x.id == tracker.trackingId);
        if (channel == undefined) {
            // delete tracker
            await DatabaseService.instance.discordTracker.delete(tracker);
        }

        return <CategoryChannel>channel;
    }

    public async getGuild(client: CommandoClient, guildId: string): Promise<Guild | undefined> {
        return client.guilds.cache.find(x => x.id === guildId);
    }

    private async getCategoryByName(client: CommandoClient, guildId: string, categoryName: string): Promise<CategoryChannel | undefined> {
        const guild = await this.getGuild(client, guildId);
        return <CategoryChannel>guild?.channels.cache.find(x => x.name === categoryName && x.type === 'category');
    }


    private async getChannelByName(client: CommandoClient, guildId: string, channelName: string, channelType: string): Promise<Channel | undefined> {
        const guild = await this.getGuild(client, guildId);
        return <CategoryChannel>guild?.channels.cache.find(x => x.name === channelName && x.type === channelType);
    }

}