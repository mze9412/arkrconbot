// Copyright (C) 2020 Mathias Zech
// This file is part of ArcRconBot <https://gitlab.com/mze9412/ArcRconBot>.
//
// ArcRconBot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArcRconBot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArcRconBot. If not, see <http://www.gnu.org/licenses/>.

import config from '../../config.json';
import { RconServerDataService } from "./database/rconServersData.service";
import { BotConfigDataService } from "./database/botConfigData.service";
import { DiscordTrackerDataService } from "./database/discordTrackerData.service";
import { RconRoleDataService } from './database/rconRoleData.service';
import { BlacklistEntryDataService } from './database/blacklistData.service';

export class DatabaseService {
    private static _instance: DatabaseService;

    public static get instance(): DatabaseService {
        if (this._instance == null) {
            this._instance = new DatabaseService(config.database.url, config.database.user, config.database.password, config.database.authSource, config.database.databaseName);
        }
        return this._instance;
    }

    private _rconServer: RconServerDataService;
    private _botConfig: BotConfigDataService;
    private _discordTracker: DiscordTrackerDataService;
    private _rconRole: RconRoleDataService;
    private _blacklist: BlacklistEntryDataService;

    private constructor(url: string, user: string, password: string, authSource: string, databaseName: string) {
        this._rconServer = new RconServerDataService(url, user, password, authSource, databaseName);
        this._botConfig = new BotConfigDataService(url, user, password, authSource, databaseName);
        this._discordTracker = new DiscordTrackerDataService(url, user, password, authSource, databaseName);
        this._rconRole = new RconRoleDataService(url, user, password, authSource, databaseName);
        this._blacklist = new BlacklistEntryDataService(url, user, password, authSource, databaseName);

        DatabaseService._instance = this;
    }

    public get rconServer(): RconServerDataService { return this._rconServer; }
    public get botConfig(): BotConfigDataService { return this._botConfig; }
    public get discordTracker(): DiscordTrackerDataService { return this._discordTracker; }
    public get rconRole(): RconRoleDataService { return this._rconRole; }
    public get blacklist(): BlacklistEntryDataService { return this._blacklist; }
    
    public async deleteAllDataForGuild(guildId: string): Promise<void> {
        await this._rconServer.deleteAll(guildId);
        await this._botConfig.deleteAll(guildId);
        await this._discordTracker.deleteAll(guildId);
        await this._rconRole.deleteAll(guildId);
        await this._blacklist.deleteAll(guildId);
    }
}