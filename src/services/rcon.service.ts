// Copyright (C) 2020 Mathias Zech
// This file is part of ArkRconBot <https://gitlab.com/mze9412/arkrconbot>.
//
// ArkRconBot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArkRconBot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArkRconBot. If not, see <http://www.gnu.org/licenses/>.

import { RconServer } from "../models/rconServer";
import { Rcon } from "rcon-client"


export class RconService {
    private static _instance: RconService;
    public static get instance(): RconService {
        if (this._instance == null) {
            this._instance = new RconService();
        }
        return this._instance;
    }

    private constructor() {}

    public async runRconCommand(server: RconServer, command: string): Promise<string>
    {
        let result = '';
        try {
            const rcon = await Rcon.connect({ host: server.ip, port: server.rconPort, password: server.password });
            result = await rcon.send(command);
            rcon.end();
        } catch(e) {
            result = `An error occured during execution of the RCON command on server *${server.name}*.`;
        }
        return result;
    }
    
    public async getPlayerList(server: RconServer): Promise<[string[], string[]]> {
        const result = await this.runRconCommand(server, 'listplayers');

        // players result list
        const players: string[] = [];
        const playersNoSteam: string[] = [];

        let playerNumber = 1;
        for (let entry of result.split('\n')) {
            // trim entry, empty entries are skipped
            entry = entry.trim();
            if (entry.length === 0) {
                continue;
            }

            // test regex and add formatted player data
            const regex = /^\d*.\s(.*),\s(\d*)/;
            const regexResult = regex.exec(entry);
            if (regexResult != null && regexResult.length === 3) {
                players.push(`${playerNumber.toFixed(0)}) __${regexResult[1]}__ (${regexResult[2]})`);
                playersNoSteam.push(`${playerNumber.toFixed(0)}) __${regexResult[1]}__`);
            }
            playerNumber++;
        }

        return [players, playersNoSteam];
    }
}