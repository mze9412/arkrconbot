// Copyright (C) 2020 Mathias Zech
// This file is part of ArcRconBot <https://gitlab.com/mze9412/arkrconbot>.
//
// ArcRconBot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArcRconBot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArcRconBot. If not, see <http://www.gnu.org/licenses/>.

export class DiscordTracker {
    guildId: string = '';
    name: string = '';
    group: string = '';
    trackingId: string = '';
    trackerType: DiscordTrackerType = DiscordTrackerType.Undefined
}

export enum DiscordTrackerType {
    Undefined,
    Channel,
    Message,
    User,
    Category
}