// Copyright (C) 2020 Mathias Zech
// This file is part of ArkRconBot <https://gitlab.com/mze9412/arkrconbot>.
//
// ArkRconBot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArkRconBot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArkRconBot. If not, see <http://www.gnu.org/licenses/>.

import { ArkRconBot } from './arkRconBot';

// create and start bot
const bot = new ArkRconBot();
bot.start();

process.on("uncaughtException", err => {
    console.log(err);
});

// register to SIGINT and SIGTERM for graceful shutdown
process.once('SIGINT', () => {
    console.info('SIGINT: Stopping');
    bot.stop();
    console.info('SIGINT: DONE');
    process.exit(0);
});
process.once('SIGTERM', () => {
    console.info('SIGTERM: Stopping');
    bot.stop();
    console.info('SIGTERM: DONE');
    process.exit(0);
});