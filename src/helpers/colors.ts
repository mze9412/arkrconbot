export abstract class Colors {
    public static green: string = '#78B159';
    public static red: string = '#DD2E44';
    public static blue: string = '#5DADEC';
    public static yellow: string = '#FDCB58';
}