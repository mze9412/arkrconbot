// Copyright (C) 2020 Mathias Zech
// This file is part of ArkRconBot <https://gitlab.com/mze9412/arkrconbot>.
//
// ArkRconBot is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// ArkRconBot is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ArkRconBot. If not, see <http://www.gnu.org/licenses/>.

import { CommandoClient } from 'discord.js-commando';
import config from '../config.json';
import { AddBlacklistEntryCommand } from './commands/addBlacklistEntryCommand';
import { AddRconRoleCommand } from './commands/addRconRoleCommand';
import { AddServerCommand } from './commands/addServerCommand';
import { CanRconCommand } from './commands/canRconCommand';
import { ConfigureSelfHostedCommand } from './commands/configureSelfHostedCommand';
import { DeleteBlacklistEntryCommand } from './commands/deleteBlacklistEntryCommand';
import { DeleteRconRoleCommand } from './commands/deleteRconRoleCommand';
import { DeleteServerCommand } from './commands/deleteServerCommand';
import { ListBlacklistEntriesCommand } from './commands/listBlacklistEntriesCommand';
import { ListRconRolesCommand } from './commands/listRconRolesCommand';
import { ListServersCommand } from './commands/listServersCommand';
import { PrintConfigurationCommand } from './commands/printConfigurationCommand';
import { RconAllCommand } from './commands/rconAllCommand';
import { RconCommand } from './commands/rconCommand';
import { DatabaseService } from './services/database.service';
import { DiscordService } from './services/discord.service';
import { ConfigureHostedChatLogCommand } from './commands/configureHostedChatLogCommand';
import { ConfigureHostedServerStatusCommand } from './commands/configureHostedServerStatusCommand';
import { StatisticsCommand } from './commands/statisticsCommand';
import { InformHostedCommand } from './commands/informHostedCommand';

export class ArkRconBot {
    private client: CommandoClient;
    private serverInfoUpdateInterval: NodeJS.Timeout | undefined;
    private chatLogUpdateInterval: NodeJS.Timeout | undefined;
    private serveInfoUpdateRunning = false;
    private chatLogUpdateRunning = false;

    constructor() {
        console.info('Creating Bot');
          this.client = new CommandoClient({
            owner: config.ownerDiscordId,
            invite: 'https://discord.gg/EKM7Na2',
            commandPrefix: '!',
            messageCacheLifetime: 30,
            messageSweepInterval: 60
        });

        this.client.registry
            .registerDefaultTypes()
            .registerDefaultGroups()
            .registerDefaultCommands({ help: true, eval: false, prefix: false, ping: false, commandState: false, unknownCommand: false })
            .registerGroups([
                ['owner', 'Special commands, only available to the bot owner specified in config file.'],
                ['admin', 'Admin-only commands'],
                ['rcon', 'Commands for rcon users'],
                ['everyone', 'Publically available commands']
            ])
            // admin commands
            .registerCommand(new DeleteServerCommand(this.client))
            .registerCommand(new AddServerCommand(this.client))
            .registerCommand(new DeleteRconRoleCommand(this.client))
            .registerCommand(new AddRconRoleCommand(this.client))
            .registerCommand(new ListRconRolesCommand(this.client))
            .registerCommand(new ListBlacklistEntriesCommand(this.client))
            .registerCommand(new AddBlacklistEntryCommand(this.client))
            .registerCommand(new DeleteBlacklistEntryCommand(this.client))
            .registerCommand(new PrintConfigurationCommand(this.client))

            // rcon commands
            .registerCommand(new ListServersCommand(this.client))
            .registerCommand(new RconCommand(this.client))
            .registerCommand(new RconAllCommand(this.client))

            // public commands
            .registerCommand(new CanRconCommand(this.client))
            ;

            if(config.isSelfHosted) {
                // commands which are only available for self-hosted
                this.client.registry
                    .registerCommand(new ConfigureSelfHostedCommand(this.client))
            } else {
                // commands which are only available for hosted
                this.client.registry
                .registerCommand(new ConfigureHostedChatLogCommand(this.client))
                .registerCommand(new ConfigureHostedServerStatusCommand(this.client))
                .registerCommand(new StatisticsCommand(this.client))
                .registerCommand(new InformHostedCommand(this.client))
            }

        // add some logging
        this.client.on('guildCreate', async guild => {
            if (config.isSelfHosted) {
                console.info(`Joined guild ${guild.name} (ID: ${guild.id}). Self-Hosted instanced can only join one guild. Checking ...`);
                const numGuilds = this.client.guilds.cache.size;
                if (numGuilds > 1) {
                    console.info('Leaving guild again, bye bye.');
                    await guild.leave();
                }
                console.info('All good! This is the only guild I am in.');
            } else {
                console.info(`Joined guild ${guild.name} (ID: ${guild.id}).`);
            }

        });
        this.client.on('guildDelete', async guild => {
            console.info(`Left guild ${guild.name} (ID: ${guild.id}).`);
            await DatabaseService.instance.deleteAllDataForGuild(guild.id);
        });
        this.client.on('guildUnavailable', guild => {
            console.info(`Guild ${guild.name} (ID: ${guild.id}) is unavailable.`);
        });
        // this.client.on('rateLimit', (info: { timeout: any; limit: any; method: any; path: any; route: any; }) => {
        //     if (!config.isSelfHosted) {
        //         const msg = `Rate Limit Hit.\nTimout: ${info.timeout}\nLimit: ${info.limit}\nMethod: ${info.method}\nPath: ${info.path}\nRoute: ${info.route}`;
        //         console.log(msg);
        //         DiscordService.instance.sendPrivateMessage(this.client, config.ownerDiscordId, msg);
        //     }
        // });
        this.client.on('ready', () => {
    	    console.info(`I am ready! Logged in as ${this.client.user?.tag}!`);
            console.info(`Bot has started, with ${this.client.users.cache.size} users, in ${this.client.channels.cache.size} channels of ${this.client.guilds.cache.size} guilds.`);

            this.client.user?.setActivity('RCON for the masses!');

            // Log all connected discords
            for (let i = 0; i < this.client.guilds.cache.size; i++) {
                const guild = this.client.guilds.cache.array()[i];
                console.info(`${i+1}) ${guild.name} (${guild.id}) Owner: ${guild.owner?.displayName} (${guild.owner?.id})`);
            }
            
            // activate server update if configured
            console.info(`Activating server info update: ${config.features.serverInfoUpdate.active}`);
            if(config.features.serverInfoUpdate.active) {
                const interval = config.features.serverInfoUpdate.interval * 1000;
                this.runServerInfoUpdate();
                this.serverInfoUpdateInterval = setInterval(() => { this.runServerInfoUpdate() }, interval);
            }
            
            // activate chat logging if configured
            console.info(`Activating chat log update: ${config.features.serverChat.active}`);
            if(config.features.serverChat.active) {
                const interval = config.features.serverChat.interval * 1000;
                this.runChatLogUpdate();
                this.chatLogUpdateInterval = setInterval(() => { this.runChatLogUpdate() }, interval);
            }
        });
        this.client.on("warn", function(info){
            console.warn(`Warning: ${info}`);
        });
    }

    // Starts the bot
    start(): void {
        if (config.isTesting) {
            console.log('### BOT IS IN TESTING MODE. DO NOT USE THIS MODE IN PRODUCTION. ###')
        }

        // login
        this.client.login(config.loginToken).then(res => {
            console.info('Login running: ' + res);
        }).catch(ex => {
            console.error(ex);
        });
    }

    // Stops the bot
    stop(): void {
        // clear server info update
        if (this.serverInfoUpdateInterval != undefined) {
            clearTimeout(this.serverInfoUpdateInterval);
        }

        // clear chat log update
        if (this.chatLogUpdateInterval != undefined) {
            clearTimeout(this.chatLogUpdateInterval);
        }

        // destroy / logout client
        this.client.destroy();
    }

    // Runs update for server info for all connected discords
    private async runServerInfoUpdate(): Promise<void> {
        // skip if update is still/already running
        if (this.serveInfoUpdateRunning) {
            return;
        }

        const start = Date.now();
        try {
            this.serveInfoUpdateRunning = true;
            await DiscordService.instance.runUpdateServerStatus(this.client);
        }
        catch(ex) {
            console.error(ex);
        } finally {
            this.serveInfoUpdateRunning = false;
            const end = Date.now();
            const duration = (end - start)/1000;
            const msg =`Server Info Update took ${duration}s.`;
            console.log(msg);
            if (duration > 300) {
                await DiscordService.instance.sendPrivateMessage(this.client, config.ownerDiscordId, msg);
            }
        }
    }

    // Runs update for server info for all connected discords
    private async runChatLogUpdate(): Promise<void> {
        // skip if update is still/already running
        if (this.chatLogUpdateRunning) {
            return;
        }

        const start = Date.now();
        try {
            this.chatLogUpdateRunning = true;
            await DiscordService.instance.runUpdateChatLog(this.client);
        }
        catch(ex) {
            console.error(ex);
        } finally {
            this.chatLogUpdateRunning = false;
            const end = Date.now();
            const duration = (end - start)/1000;
            const msg =`Chat Logging took ${duration}s.`;
            console.log(msg);
            if (duration > 60) {
                await DiscordService.instance.sendPrivateMessage(this.client, config.ownerDiscordId, msg);
            }
        }
    }
}