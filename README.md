# ArkRconBot

This is a Discord bot geared towards executing arbitrary RCON commands on remote ARK servers.

## Requirements

- NodeJS
- MongoDB

Only host this yourself if you know what you are doing!

## Usage & Support

For feedback, questions and anything else please join the support Discord server: <https://discord.gg/EKM7Na2>

If you want to use the hosted version of this bot, you can have it join your server with this link: <https://discord.com/oauth2/authorize?client_id=758701893148737597&scope=bot&permissions=8>

If you like the bot, please send me a small donation on PayPal: <https://paypal.me/mze9412>

## Support me

If you just want to say thank you or support me, join the support Discord and send me a private message or drop me a small "thank you" on PayPal: <https://paypal.me/mze9412>

## Custom bot

If you require a custom Discord bot, hit me up on Discord on what you need. Maybe we can find a solution.

## Changelog

### 0.1.0 - Initial release

- adding of servers
- management of Discord roles which are allowed to run RCON commands
- execution of RCON commands on single servers or multiple servers

### 0.1.1 - Blacklisting

- blacklist strings, any RCON command which contains a blacklisted string is refused

### 0.1.2 - Server Status

- Server status display via automatically generated voice channels. Needs to be set up with the *!configure* command.

### 0.2.0 - Public release

- Licensed under GPLv3
- All features available for self-hosted setup
- Limited features available for hosted setup, additional features unlocked via Patreon (to cover hosting costs)

### 0.2.1 - Small fix

- New config parameter *authSource* for MongoDB. It was formerly hardcoded as *RconBot*, which limited usage with own MongoDB instances. See *config.example.json* on where to place it.

### 0.2.6 - Chat Logging

- configuration is split now
*Self-Hosted*
!configure

*Hosted*
!configure-chatlog
!configure-serverstatus

- config file updated and better structured for self-hosting
- Server Status Update can now RECREATE or EDIT channels, depending on your choice and wish
- chat logging can seperate admin and tribe logs into own channels if required (global config, only changable for self hosted)

- the bot now creates the categories and channels on its own
- category permissions and channel permissions can be edited afterwards (except for status channels, if you are using RECREATE)
- hosted version uses EDIT strategy for now

### 0.2.7 - Player List

It is not possible to show a list of all online players. This is a sub-feature of server status update and only works if that is also enabled.
Run *!configure-serverstatus* or *!configure*, depending on your hosting state, to configure the feature.

The generated channel is not visible for normal users but you can modify the permissions after the bot created it.

### 0.2.8 - Bufixes

Several small bug fixes

### 1.0.0 - Technical refactoring

- Refactored internal namings and how the bot tracks channels and messages for updating/editing
- Switched to semantic versioning

### 1.1.0 - Change player listing

- avoid embed message getting too large to quickly ([Issue](https://gitlab.com/mze9412/arkrconbot/-/issues/3))
- simplify status update code by splitting it up

### 1.1.1 - Formatting fixes

- very small formatting updates for player listing

### 2.0.0 - Upgrade dependencies

- upgrade to latest typescript version
- upgrade discord and mongodb dependencies

This is a breaking change! You need to upgrade your typescript version and all packages of the project!

### 2.1.0 - Tribelog now shows map

- tribelog shows map name
- players in list are numbered
